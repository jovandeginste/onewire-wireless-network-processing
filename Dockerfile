FROM scratch
ARG SUFFIX
COPY bin/receive.${SUFFIX} /receive
CMD [ "/receive", "/config.yaml" ]
