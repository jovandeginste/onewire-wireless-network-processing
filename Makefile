BINARY=receive
IMAGE=jovandeginste/onewire-wireless-network-processing

ALL_ARCHS=amd64 rpi rpi2

.PHONY: all clean test

all:
	-$(MAKE) deps
	$(MAKE) build-all

test:
	golangci-lint run --fix --new=true --color=always

deps:
	go get

docker-all: $(foreach arch,$(ALL_ARCHS),docker-$(arch))
build-all: $(foreach arch,$(ALL_ARCHS),build-$(arch))

build:
	go build -o "bin/$(BINARY).$(SUFFIX)"
	file "bin/$(BINARY).$(SUFFIX)"

build-amd64:
	GOARCH=amd64 GOOS=linux SUFFIX=$(subst build-,,$(@)) $(MAKE) build

build-rpi:
	GOARCH=arm GOARM=6 SUFFIX=$(subst build-,,$(@)) $(MAKE) build

build-rpi2:
	GOARCH=arm GOARM=7 SUFFIX=$(subst build-,,$(@)) $(MAKE) build

docker-rpi:
	SUFFIX=$(subst docker-,,$(@)) PLATFORM=linux/arm/v6 $(MAKE) docker

docker-rpi2:
	SUFFIX=$(subst docker-,,$(@)) PLATFORM=linux/arm/v7 $(MAKE) docker

docker-amd64:
	SUFFIX=$(subst docker-,,$(@)) PLATFORM=linux/amd64 $(MAKE) docker

docker:
	docker buildx build --build-arg SUFFIX=$(SUFFIX) --platform=$(PLATFORM) -f Dockerfile -t $(IMAGE):latest-$(SUFFIX) --push .
